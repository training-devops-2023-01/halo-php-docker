# Aplikasi PHP dengan Docker #

1. Menjalankan aplikasi

    ```
    php -S localhost:8000
    ```

2. Menjalankan postgresql docker image

    ```
    docker run --name coba-postgres -e POSTGRES_USER=belajar -e POSTGRES_PASSWORD=docker -e POSTGRES_DB=halo-php -p 9000:5432 postgres:15-alpine
    ```

3. Menggunakan volume mapping supaya data postgres tidak hilang pada saat container dihapus

    ```
    docker run --name coba-postgres -e POSTGRES_USER=belajar -e POSTGRES_PASSWORD=docker -e POSTGRES_DB=halo-php -p 9000:5432 -v /Users/endymuhardin/workspace/training/training-devops-2023-01/halo-php-docker/db-halo:/var/lib/postgresql/data postgres:15-alpine
    ```

4. Membuat tabel di database

    ```sql
    create table tamu(
        id serial,
        nama varchar(100) not null,
        email varchar(100) not null,
        pesan varchar(255),
        primary key (id),
        unique(email)
    );
    ```

5. Menjalankan postgresql dengan `docker-compose`

    ```
    docker-compose up
    ```

    Untuk menghapus container

    ```
    docker-compose down
    ```

6. Membuat docker image dari `Dockerfile` dengan nama image adalah `halo-php`

    ```
    docker build -t endymuhardin/halo-php .
    ```

7. Jalankan docker image `halo-php` yang barusan dibuat

    ```
    docker run -p 7000:80 endymuhardin/halo-php
    ```

    Setelah itu browse ke [http://localhost:7000/info.php](http://localhost:7000/info.php)

8. Upload ke Docker Hub

    ```
    docker push endymuhardin/halo-php
    ```